import React from 'react';
import './App.css';
import Board from './components/board.component';
import firebase from 'firebase';
import ReactFireMixin from 'reactfire';


let config = {
    apiKey: "AIzaSyAdWgzaEGijLTMbrvrUpkDPqdA-jIkVkCk",
    authDomain: "brickbet-test-justin.firebaseapp.com",
    databaseURL: "https://brickbet-test-justin.firebaseio.com",
    projectId: "brickbet-test-justin",
    storageBucket: "brickbet-test-justin.appspot.com",
    messagingSenderId: "641693840684"
};
firebase.initializeApp(config);


let App = React.createClass({
    mixins: [ReactFireMixin],

    getInitialState: function () {
        return {
            board: []
        };
    },
    componentWillMount: function () {
        let ref = firebase.database().ref('board');
        this.bindAsObject(ref, 'board');
    },

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    {/*<img src={logo} className="App-logo" alt="logo" />*/}
                    <h2>BrickBet <span className="h6">(beta)</span></h2>
                </div>
                <Board data={this.state.board}></Board>
            </div>
        );
    }
});

export default App;

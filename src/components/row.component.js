import React, { Component } from 'react';
import './row.styles.css'
import Square from "./square.component";

class GridRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            numCols: props.numCols,
            rowId: props.rowId
        };
    }

    generateColumns = () =>{
        let cols = [];
        for (let colId = 1; colId <= this.state.numCols; colId++) {
            cols.push(this.generateSquare(this.state.rowId, colId));
        }
        return cols;
    };

    generateSquare = (rowId, colId) => {
        return <Square rowId={rowId} colId={colId} key={"square" + rowId+':'+colId}/>;
    };

    render() {
        return (
            <div className="Row">
                {this.generateColumns()}
            </div>
        );
    }
}

export default GridRow;
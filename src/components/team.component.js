import React, { Component } from 'react';
import './team.styles.css';

class Team extends Component {

    constructor(props) {
        super(props);
        this.state = {
            teamName: props.teamName,
            isHomeTeam: props.isHomeTeam
        };
    }

    render() {
        return (
            <div className={'Team ' + (this.state.isHomeTeam ? 'HomeTeam' : 'VisitingTeam')}>
                {this.state.teamName}
            </div>
        );
    }
}

export default Team;
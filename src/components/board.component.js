import React, {Component} from 'react';

import './board.styles.css'
import './team.styles.css'
import './row.styles.css'
import './grid.styles.css';
import './square.styles.css'

let Table = require('react-bootstrap').Table;
const today = new Date();
let gameClose = new Date();

class Board extends Component {

    generateGameRows = () => {
        gameClose = Date.parse(this.props.data.gameClose);

        let rows = [];
        for (let rowId = 1; rowId <= this.props.data.numRows + 1; rowId++) {
            if (rowId <= this.props.data.numRows || (rowId > this.props.data.numRows && this.props.data.scores && this.props.data.scores.home && this.props.data.scores.home.length === this.props.data.numRows && today > gameClose)) {
                rows.push(
                    <div className="Row" key={"row" + rowId}>
                        {this.generateColumns(rowId)}
                    </div>
                );
            }
        }
        return rows;
    };

    generateColumns = (rowId) => {
        let cols = [];
        for (let colId = 1; colId <= this.props.data.numCols + 1; colId++) {
            if (colId <= this.props.data.numCols || (colId > this.props.data.numCols && this.props.data.scores && this.props.data.scores.visitor && this.props.data.scores.visitor.length === this.props.data.numCols && today > gameClose)) {
                cols.push(this.generateSquare(rowId, colId));
            }
        }
        return cols;
    };

    generateSquare = (rowId, colId) => {
        return (
            <div
                className={this.getSquareClass(rowId, colId)}
                key={"square" + rowId + "x" + colId}>
                {this.getSquareData(rowId, colId)}
            </div>
        );
    };

    getSquareClass = (rowId, colId) => {
        var _class = "Square";
        if (rowId > this.props.data.numRows || colId > this.props.data.numCols) {
            if (rowId <= this.props.data.numRows || colId <= this.props.data.numCols) {
                _class += " Score";
            }
        }
        else if (this.getSquareData(rowId, colId) && this.getSquareData(rowId, colId) !== "") {
            _class += " SquareTaken";
        }

        return _class;
    };

    getSquareData = (rowId, colId) => {
        if (rowId <= this.props.data.numRows && colId <= this.props.data.numCols) {
            try {
                return this.props.data.picks[this.getSquareId(rowId, colId)];
            } catch (Exc) {
                return "";
            }
        }
        else if (rowId > this.props.data.numRows && colId <= this.props.data.numCols) {
            return this.props.data.scores.home[colId - 1];
        }
        else if (rowId <= this.props.data.numRows && colId > this.props.data.numCols) {
            return this.props.data.scores.visitor[rowId - 1];
        }
    };

    getSquareId = (rowId, colId) => {
        return "row" + rowId + "col" + colId;
    };

    homeTeam = () => {
        try {
            return this.props.data.teams.home;
        } catch (Exc) {
            return "";
        }
    };

    visitingTeam = () => {
        try {
            return this.props.data.teams.visitor;
        } catch (Exc) {
            return "";
        }
    };


    render() {
        return (
            <div className="Board">
                <Table responsive={true}>
                    <tbody>
                    <tr>
                        <td className="LayoutOriginSpacer">&nbsp;</td>
                        <td className="Team">
                            <div className="HomeTeam">{this.homeTeam()}</div>
                        </td>
                    </tr>
                    <tr>
                        <td className="Team">
                            <div className="VisitingTeam">{this.visitingTeam()}</div>
                        </td>
                        <td>
                            {/*<BoardGrid numRows={this.props.data.numRows} numCols={this.props.data.numCols} this.props.data.scores={this.props.data.scores}></BoardGrid>*/}
                            <div className="Grid">
                                {this.generateGameRows()}
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Board;
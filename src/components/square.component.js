import React, { Component } from 'react';
import './square.styles.css'

class Square extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rowId: props.rowId,
            colId: props.colId
        };
        console.log("new square " + JSON.stringify(this.state));
    }

    render() {
        return (
            <div className={(this.state.rowId === 0 || this.state.colId === 0) ? "Square SquareHeader" : "Square"}>
                {this.state.rowId} x {this.state.colId}
            </div>
        );
    }
}

export default Square;
import React, { Component } from 'react';
import './grid.styles.css';
import GridRow from "./row.component";


class BoardGrid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            numRows: props.numRows,
            numCols: props.numCols
        };
    }

    generateGameRows = () => {
        let rows = [];
        for (let rowId = 1; rowId <= this.state.numRows; rowId++) {
            rows.push(<GridRow rowId={rowId} numCols={this.state.numCols} key={"row" + rowId}/>);
        }
        return rows;
    };

    render() {
        return (
            <div className="Grid">
                {this.generateGameRows()}
            </div>
        );
    }
}

export default BoardGrid;